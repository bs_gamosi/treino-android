package com.gmsil.aula32;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.TextView;

public class segunda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        Intent i= getIntent();
        String msg = i.getStringExtra("texto");

        TextView txt = findViewById(R.id.text);

        txt.setText(msg);
    }

    public void telaVolta(View view) {
        Intent it = new Intent( this, segunda.class );
        TextInputEditText txtInput = findViewById(R.id.nome_edit);
        String manda = txtInput.getText().toString();
        it.putExtra("txt", manda);
        setResult(RESULT_OK, it);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
    }
}
