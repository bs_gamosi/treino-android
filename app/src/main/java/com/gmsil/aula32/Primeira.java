package com.gmsil.aula32;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class Primeira extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.primeira_activity);

    }

    public void proxTela(View view) {
        Intent it = new Intent( this, segunda.class );
        //Bundle bd = new Bundle();

        TextInputEditText edit = findViewById(R.id.txtManda_edit);
        it.putExtra("texto", edit.getText().toString());
        startActivityForResult(it,5);
    }

    public void telaThread(View view){
        Intent it = new Intent( this, Threds.class );
        startActivity(it);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        Log.d("App", "OnCreate()");
        //super.onActivityResult(requestCode,resultCode,data);
        if (requestCode == 5){
            String msg = data.getStringExtra("txt");
            TextView txtV = findViewById(R.id.textView);
            txtV.setText(msg);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("App", "OnDestroy()");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("App", "OnStart()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("App", "OnStop()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("App", "OnRestart()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("App", "OnResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("App", "OnPause()");
    }
}
