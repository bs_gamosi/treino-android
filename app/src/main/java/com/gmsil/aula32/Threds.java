package com.gmsil.aula32;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Threds extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threds);
    }

    public void voltabt(View view){
        setResult(RESULT_CANCELED);
        finish();
    }

    public void bagunsinha(View view){
        Runnable r = new Runnable() {

            @Override
            public void run() {
                final TextView txtV = findViewById(R.id.textV);
                final Button bt = findViewById(R.id.botaoProcessa);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bt.setText("Processando");
                    }
                });

                for(int cont=1;cont<=20;cont++){
                    SystemClock.sleep(1000);
                    final String q = "contando... "+cont;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            txtV.setText(q);
                        }
                    });
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        bt.setText("Processar");
                        txtV.setText("Contagem finalizada!!!");
                    }
                });

            }
        };
        Thread t = new Thread(r);
        t.start();
    }

    public void manual(View view){
        TextView txtV = findViewById(R.id.manualV);
        int a = (Integer.parseInt(txtV.getText().toString()));
        int aux = a + 1;
        txtV.setText(""+aux);
    }


}
